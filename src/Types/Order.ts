import type Employee from "./Employee";
import type OrderItem from "./Order-item";
import type Customer from "./customer";
import type Store from "./store";

export interface Order {
  id?: number;

  amount?: number;

  total?: number;

  received?: number;

  change?: number;

  payment?: string;

  discount?: number;

  createdDate?: Date;

  updatedDate?: Date;

  deletedDate?: Date;

  employeeId?: Employee;

  customerId?: Customer;

  storeId?: Store;

  orderItems?: OrderItem[];
}
