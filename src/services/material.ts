import http from "./axios";
import type Materials from "@/Types/material";
function getMaterials() {
  return http.get("/materials");
}

function saveMaterial(material: Materials) {
  return http.post("/materials", material);
}

function updateMaterial(id: number, material: Materials) {
  return http.patch(`/materials/${id}`, material);
}

function deleteMaterial(id: number) {
  return http.delete(`/materials/${id}`);
}

export default {
  saveMaterial,
  getMaterials,
  updateMaterial,
  deleteMaterial,
};
