export default interface Customer {
  id?: number;
  codeCus?: string;
  name: string;
  phone: string;
  points: number;
}
