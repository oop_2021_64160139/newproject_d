import type Store from "@/Types/store";
import { defineStore } from "pinia";
import { ref } from "vue";
import StoreService from "@/services/store";
import { useMessageStore } from "./message";
export const useStore = defineStore("store", () => {
  const messageStore = useMessageStore();
  const store = ref<Store[]>([]);
  const editedStore = ref<Store>({
    name: "",
    address: "",
    tel: "",
  });
  async function getStore() {
    try {
      const res = await StoreService.getStores();
      store.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Store ได้");
    }
  }
  return {
    store,
    editedStore,
    getStore,
  };
});
