import { ref } from "vue";
import { defineStore } from "pinia";
import { useMessageStore } from "./message";
import type SummarySalary from "@/Types/summary-salary";
import summarysalary from "@/services/summarysalary";

export const useSummarySalaryStore = defineStore("summarysalary", () => {
  const dialogsum = ref(false);
  const dialogsalary = ref(false);
  const messageStore = useMessageStore();
  const email = ref();

  return {
    dialogsum,
    dialogsalary,
    messageStore,
  };
});
