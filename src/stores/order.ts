import { defineStore } from "pinia";
import { computed, ref } from "vue";
import { useCoffeeStore } from "./menu";
import OrderService from "@/services/order";
import { useMessageStore } from "./message";
import { useCustomerStore } from "./customer";
import type Product from "@/Types/Product";
import { useLoadingStore } from "./loading";
import { useAuthStore } from "./auth";
import type { Order } from "@/Types/Order";


export const useOrderStore = defineStore("order", () => {
  const change = ref<number>(0);
  const dialog = ref();
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const authStore = useAuthStore();
  const orders = ref<Order[]>([]);
  const useCustomer = useCustomerStore();
  const month = ref<{month:string ; total_amount:number}[]>([]);
  const yearly = ref<{year : string ; total_amount:number}[]>([]);
  const best = ref<{name : string; total_amount : number}[]>([]);
  const worst = ref<{name : string; total_amount : number}[]>([]);
  const orderList = ref<{ product: Product; amount: number; total: number }[]>(
    []
  );
  function addProduct(item: Product) {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].product.id === item.id) {
        orderList.value[i].amount++;
        orderList.value[i].total = orderList.value[i].amount * item.price;
        return;
      }
    }
    orderList.value.push({ product: item, amount: 1, total: 1 * item.price });
  }
  function clearOrder() {
    orderList.value = [];
  }
  function deleteProduct(index: number) {
    orderList.value.splice(index, 1);
  }
  const changeCal = computed((receivedCash: number) => {
    return (change.value = receivedCash - sumPrice.value);
  });
  function increment(id: number) {
    orderList.value[id].amount++;
    orderList.value[id].total =
      orderList.value[id].amount * orderList.value[id].product.price;
    return;
  }
  function decrement(id: number) {
    orderList.value[id].amount--;
    orderList.value[id].total =
      orderList.value[id].amount * orderList.value[id].product.price;
    if (orderList.value[id].amount === 0) {
      deleteProduct(id);
    }
    return;
  }
  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].total;
    }
    return sum;
  });
  const sumAmount = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].amount;
    }
    return sum;
  });
  async function openOrder() {
    if (orderList.value.length === 0) {
      return;
    }else{
    const user: { id: number } = authStore.getUser();
    const orderItems = orderList.value.map(
      (item) =>
        <
          {
            productId: number;
            amount: number;
          }
        >{
          productId: item.product.id,
          amount: item.amount,
        }
    );

    const order = {
      orderItems: orderItems,
      payment: "เงินสด",
      // received: 100,
      // tel: "0864945419",
      storeId: 1,
      employeeId: user.id,
      customerId: 1,
    };
    loadingStore.isLoading = true;
    try {
      const res = await OrderService.saveOrder(order);
      dialog.value = false;
      clearOrder();
      await getOrders();
    } catch (e) {
      messageStore.showMessage("ไม่สามารถบันทึก Order ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  }

  async function openOrderQR() {
    if (orderList.value.length === 0) {
      return;
    }else{
    const user: { id: number } = authStore.getUser();
    const orderItems = orderList.value.map(
      (item) =>
        <
          {
            productId: number;
            amount: number;
          }
        >{
          productId: item.product.id,
          amount: item.amount,
        }
    );

    const order = {
      orderItems: orderItems,
      payment: "QRCode",
      // received: 100,
      // tel: "0864945419",
      storeId: 1,
      employeeId: user.id,
      customerId: 1,
    };
    loadingStore.isLoading = true;
    try {
      const res = await OrderService.saveOrder(order);
      dialog.value = false;
      clearOrder();
      await getOrders();
    } catch (e) {
      messageStore.showMessage("ไม่สามารถบันทึก Order ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  }
  async function getOrders() {
    try {
      const res = await OrderService.getOrders();
      orders.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Customers ได้");
    }
  }

  async function getOrderItem_info_DESC() {

    
    try {
      const res = await OrderService.getOrderItem_info_DESC();
      best.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Customers ได้");
    }
  }

  
  async function getOrderItem_info_ASC() {

    
    try {
      const res = await OrderService.getOrderItem_info_ASC();
      worst.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Customers ได้");
    }
  }

  async function getMonthly() {
    console.log(month);
    
    try {
      const res = await OrderService.getMonthly();
  
      month.value = res.data
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Customers ได้");
    }
  }

  async function getYearly() {
    
    try {
      const res = await OrderService.getYearly();
      yearly.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Customers ได้");
    }
  }

  return {
    orderList,
    addProduct,
    deleteProduct,
    sumPrice,
    sumAmount,
    openOrder,
    clearOrder,
    getOrders,
    orders,
    increment,
    decrement,
    change,
    changeCal,
    openOrderQR,
    getOrderItem_info_ASC,
    getOrderItem_info_DESC,
    best,
    worst,
    getMonthly,
    month,
    getYearly,
    yearly
  };
});
