import type Product from "@/Types/Product";
import { defineStore } from "pinia";
import product from "@/services/product";
import { ref } from "vue";
import { useMessageStore } from "./message";
import { computed } from "vue";
import ProductService from "@/services/product";
import { watch } from "vue";

export const useProductStore = defineStore("Product", () => {
  const dialog = ref(false);
  const dialogCheck = ref(false);
  const messageStore = useMessageStore();
  const search = ref("");
  const temp = ref();
  //const productcat = ref<Product[]>([]);
  const Products = ref<Product[]>([]);
  const editedProduct = ref<Product>({
    name: "",
    price: 0,
    type: "",
    size: "",
    img: "",
    categoryId: 0,
  });

  const clear = () => {
    editedProduct.value = {
      name: "",
      price: 0,
      type: "",
      size: "",
      categoryId: 0,
      img: "",
    };
  };
  async function getProducts() {
    try {
      const res = await product.getProducts();
      Products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Product ได้");
    }
  }

  async function deleteProduct(id: number) {
    try {
      const res = await product.deleteProduct(id);
      await getProducts();
    } catch (e) {
      console.log(e);
    }
  }

  const Productshow = computed(() => {
    return Products.value.filter((item) => {
      return (item.name || "")
        .toLowerCase()
        .match((search.value || "").toLowerCase());
    });
  });

  async function saveProduct() {
    try {
      const res = await ProductService.getProducts();
      const currentProduct = res.data;
      const saveRes = await ProductService.saveProduct(editedProduct.value);
      dialog.value = false;
      await getProducts();
      clear();
    } catch (e) {
      //
    }
  }

  watch(temp, (newTemp) => {
    if (newTemp) {
      editedProduct.value = {
        id: temp.value.id,
        name: temp.value.name,
        type: temp.value.type,
        price: temp.value.price,
      };
    }
  });

  async function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    if (editedProduct.value.id) {
      const res = await ProductService.updateProduct(
        editedProduct.value.id,
        editedProduct.value
      );
    }
  }

  return {
    getProducts,
    Products,
    editedProduct,
    deleteProduct,
    Productshow,
    saveProduct,
    editProduct,
    search,
    dialog,
    dialogCheck,
    clear,
    temp,
  };
});
