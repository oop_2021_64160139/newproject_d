import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/Types/Employee";
import EmployeeService from "@/services/employee";
import { useMessageStore } from "@/stores/message";

export const useEmployeeStore = defineStore("employee", () => {
  const dialog = ref(false);

  const messageStore = useMessageStore();
  const editedEmployee = ref<Employee>({
    name: "",
    address: "",
    phone: "",
    email: "",
    position: "",
    hourly_wage: 0,
  });
  const employeeList = ref<Employee[]>([]);

  const deleteEmployee = async (id: number): Promise<void> => {
    try {
      const res = await EmployeeService.deleteEmployee(id);
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถลบ Employee ได้");
    }
  };

  // async function saveEmployee() {
  //   console.log(editedEmployee);
  //   try {
  //     if (editedEmployee.value.id) {
  //       const res = await EmployeeService.updateEmployee(
  //         editedEmployee.value.id,
  //         editedEmployee.value
  //       );
  //     } else {
  //       const res = await EmployeeService.saveEmployee(editedEmployee.value);
  //     }
  //     dialog.value = false;
  //     await getEmployees();
  //   } catch (e) {
  //     messageStore.showMessage("ไม่สามารถบันทึก Employee ได้");
  //   }
  // }
  async function saveEmployee() {
    console.log(editedEmployee);

    try {
      // Get the current list of employees
      const res = await EmployeeService.getEmployees();
      const currentEmployees = res.data;
      // Save or update the employee depending on whether it has an id or not
      if (editedEmployee.value.id) {
        const res = await EmployeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        const res = await EmployeeService.saveEmployee(editedEmployee.value);
      }

      dialog.value = false;
      await getEmployees();
    } catch (e) {
      messageStore.showMessage("ไม่สามารถบันทึก Employee ได้");
    }
  }

  const editEmployee = (employee: Employee) => {
    editedEmployee.value = { ...employee };
    dialog.value = true;
  };

  const clear = () => {
    editedEmployee.value = {
      name: "",
      address: "",
      phone: "",
      email: "",
      position: "",
      hourly_wage: 0,
    };
  };

  async function getEmployees() {
    try {
      const res = await EmployeeService.getEmployees();
      employeeList.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Employee ได้");
    }
  }

  async function getEmployees_info() {
    try {
      const res = await EmployeeService.getEmployee_info();
      employeeList.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Employee ได้");
    }
  }
  return {
    employeeList,
    deleteEmployee,
    dialog,
    editedEmployee,
    clear,
    saveEmployee,
    editEmployee,
    getEmployees,
    getEmployees_info,
  };
});
