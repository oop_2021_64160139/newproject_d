export default interface Store {
  id?: number;
  name: string;
  address: string;
  tel: string;
}
