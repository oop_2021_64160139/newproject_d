export interface Receipt {
  id?: number;
  queue: number;
  date: string;
  time: string;
  discount: number;
  total: number;
  received: number;
  change: number;
  payment: string;
}
