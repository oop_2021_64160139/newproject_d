export default interface Product {
  id?: number;
  name: string;
  type: string;
  size: string;
  price: number;
  img: string;
  categoryId: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
