import type { Order } from "@/Types/Order";
import http from "./axios";

function getOrders() {
  return http.get("/orders");
}

function saveOrder(order: {
  orderItems: { productId: number; amount: number }[];
  payment: string;
  // received: number;
  // tel: string;
  storeId: number;
  employeeId: number;
  customerId: number;
}) {
  return http.post("/orders", order);
}

function updateOrder(id: number, order: Order) {
  return http.patch(`/orders/${id}`, order);
}

function deleteOrder(id: number) {
  return http.delete(`/orders/${id}`);
}

function getOrderItem_info_DESC() {
  return http.get("/reports/OrderItem_view_desc");
}

function getOrderItem_info_ASC() {
  return http.get("/reports/OrderItem_view_asc");
}
function getMonthly() {
  return http.get("/reports/Monthly");
}
function getYearly() {
  return http.get("/reports/Yearly");
}
export default {
  getOrders,
  saveOrder,
  updateOrder,
  deleteOrder,
  getOrderItem_info_DESC,
  getOrderItem_info_ASC,
  getMonthly,
  getYearly,
};
