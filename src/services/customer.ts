import type Customer from "@/Types/customer";
import http from "./axios";
function getCustomers() {
  return http.get("/customers");
}

function saveCustomer(customer: Customer) {
  return http.post("/customers", customer);
}

function updateCustomer(id: number, customer: Customer) {
  return http.patch(`/customers/${id}`, customer);
}

function deleteCustomer(id: number) {
  return http.delete(`/customers/${id}`);
}

function getCustomer_info() {
  return http.get("/reports/customer_view");
}

function getCustomerSP() {
  return http.get("/reports/customerSP");
}

export default {
  getCustomerSP,
  getCustomer_info,
  getCustomers,
  saveCustomer,
  updateCustomer,
  deleteCustomer,
};
