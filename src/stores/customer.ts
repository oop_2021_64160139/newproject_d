import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Customer from "@/Types/customer";
import CustomerService from "@/services/customer";
import { useMessageStore } from "./message";
export const useCustomerStore = defineStore("customer", () => {
  const dialog = ref(false);
  const messageStore = useMessageStore();
  const customer = ref<Customer[]>([]);
  const customerSP = ref<Customer[]>([]);
  const editedCustomer = ref<Customer>({
    name: "",
    phone: "",
    points: 0,
  });
  const clear = () => {
    editedCustomer.value = {
      name: "",
      phone: "",
      points: 0,
    };
  };

  async function saveCustomer() {
    try {
      // Get the current list of customers
      const res = await CustomerService.getCustomers();
      const currentCustomers = res.data;

      // Save the customer
      const saveRes = await CustomerService.saveCustomer(editedCustomer.value);
      dialog.value = false;
      await getCustomers();
    } catch (e) {
      messageStore.showMessage("ไม่สามารถบันทึก Customers ได้");
    }
  }

  const deleteCustomer = async (id: number): Promise<void> => {
    try {
      const res = await CustomerService.deleteCustomer(id);
      await getCustomers();
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถลบ Customers ได้");
    }
  };
  async function getCustomers() {
    try {
      const res = await CustomerService.getCustomers();
      customer.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Customers ได้");
    }
  }

  async function getCustomer_info() {
    try {
      const res = await CustomerService.getCustomer_info();
      customer.value = res.data;
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Customers ได้");
    }
  }

  async function getCustomerSP() {
    try {
      const res = await CustomerService.getCustomerSP();
      customer.value = res.data[0];
    } catch (e) {
      messageStore.showMessage("ไม่สามารถดึงข้อมูล Customers ได้");
    }
  }

  function editCus(customer: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
  }
  const checkPhone = (phone: string) => {
    const check = customer.value.find((c) => c.phone === phone);
    return check;
  };
  return {
    customer,
    editCus,
    dialog,
    editedCustomer,
    clear,
    saveCustomer,
    deleteCustomer,
    getCustomers,
    checkPhone,
    getCustomer_info,
    getCustomerSP,
  };
});
