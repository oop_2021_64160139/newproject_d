import type { Category } from "@/Types/Category";
import http from "./axios";
function getCategory() {
  return http.get("/category");
}

function saveCategory(category: Category) {
  return http.post("/category", category);
}

function updateCategory(id: number, category: Category) {
  return http.patch(`/category/${id}`, category);
}

function deleteCategory(id: number) {
  return http.delete(`/category/${id}`);
}
export default { getCategory, saveCategory, updateCategory, deleteCategory };
